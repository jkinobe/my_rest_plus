from flask_restful import Resource, reqparse
from flask_jwt import jwt_required

from models.item import Itemmodel


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('price',
        type=float,
        required=True,
        help="cant be left blank"
        )
    parser.add_argument('store_id',
        type=float,
        required=True,
        help="every item needs a store id."
        )


    @jwt_required()
    def get(self, name):
        item = Itemmodel.find_by_name(name)
        if item:
            return item.json()
        return {'message': "item not found"}, 404


    def post(self, name):

        if Itemmodel.find_by_name(name):#checking if item is there can use item.find_by_name(name)
            return {'message':" An item with name '{}' already exists.".format(name)}, 400
###############
        data = Item.parser.parse_args()

        item = Itemmodel(name, **data)
        #item = Itemmodel(name, data['price'], data['store_id']) the same as that above
        try:
            item.save_to_db()
        except:
            return {"message": "Error occured insering item."}, 500

        return item.json(), 201


    def delete(self, name):
        item = Itemmodel.find_by_name(name)
        if item:
            item.delete_from_db()

        return {'message': 'Item deleted'}

    def put(self, name):
        data = Item.parser.parse_args()

        item = Itemmodel.find_by_name(name) #check if item exists


        if item is None:
            item = Itemmodel(name, **data)

        else:
            item.price = data['price']

        item.save_to_db()
        return item.json()



class ItemList(Resource):
    def get(self):

        return {'items': [item.json() for item in Itemmodel.query.all()]}
        #{'items': list(map(lambda x: x.json(), Itemmodel.query.all()))}
