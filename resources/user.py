import sqlite3
from flask_restful import Resource, reqparse
from models.user import UserModel


class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username',
        type=str,
        required=True,
        help="This cant be blank"

    )
    parser.add_argument('password',
        type=str,
        required = True,
        help="This cant be blank"

    )
    def post(self):
        data = UserRegister.parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return {"Message":"USER ALREADY EXISTS"}, 401

        user = UserModel(**data)# for each of users in data username = username and password  = password
        user.save_to_db()

        return {'Message': "USER CREATED SUCCESS"}, 201
