from flask_restful import Resource
from models.store import Storemodel

class Store(Resource):

    def get(self,name):
        store = Storemodel.find_by_name(name)
        if store:
            return store.json()
        return {'Message':'Store not found'}, 404


    def post(self,name):
        store = Storemodel.find_by_name(name)
        if store:
            return {'message': "store '{}' already there.".format(name)}, 400
        store = Storemodel(name)
        try:
            store.save_to_db()
        except:
            return {'message': 'Error occured creating store'}, 400

        return store.json(), 201

    def delete(self,name):
        store = Storemodel.find_by_name(name)
        if store:
            store.delete_from_db()
        return {'message': 'Store deleted'}


class Storelist(Resource):
    def get(self):
        return {'stores': [store.json() for store in Storemodel.query.all()]}
