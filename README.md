# My_Rest_Plus

restful apis for you and me

## Introduction
Prior Knowledge about Flask in python

## Installation

```
pip install Flask
pip install -r requirements.txt
python app.py
```

## Description
This is my first sample restful api app that demonstrates a sample store management application

## Implementation
It shows how users can be able to create accounts, add, delete, update items in the store using the sqlalchemy database in flask
Simply a CRUD api

## Contributer:
Kinobe Jordan
